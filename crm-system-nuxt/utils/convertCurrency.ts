export const convertCurrency = (amount: string | number) => {
    return new Intl.NumberFormat("en-IN", {
        style: 'currency',
        currency: 'USD',
    }).format(+amount)
}