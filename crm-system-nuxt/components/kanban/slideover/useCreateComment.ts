import {useMutation} from "@tanstack/vue-query";
import {v4 as uuid} from 'uuid';
import {DB_ID, COLLECTION_COMMENTS} from "../../../app.constants";
import {useDealSliderStore} from "../../../store/deal-slider.store";

export function useCreateComments({refetch}: {refetch: () => void}) {
    const store = useDealSliderStore()
    const commentRef = ref<string>()

    const {mutate} = useMutation({
        mutationKey: ['add comments', commentRef.value],
        mutationFn: () => DB.createDocument(DB_ID, COLLECTION_COMMENTS, uuid(), {
            text: commentRef.value,
            deal: store.card?.id,
        }),
        onSuccess: () => {
            refetch()
            commentRef.value = ''
        }
    })

    const writeComment = () => {
        if(!commentRef.value) return
        mutate()
    }

    return {
        writeComment,
        commentRef
    }
}