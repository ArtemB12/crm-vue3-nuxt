import {useQuery} from "@tanstack/vue-query";
import {DB_ID, COLLECTION_DEALS} from "../../../app.constants";
import {useDealSliderStore} from "../../../store/deal-slider.store";

export function useComments() {
    const store = useDealSliderStore()
    const cardId = store.card?.id || ''

    return useQuery({
        queryKey: ['deal', cardId],
        queryFn: () => DB.getDocument(DB_ID, COLLECTION_DEALS, cardId)
    })
}